package kmitl.ce.smart_music_player.network;

import java.util.List;

import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.models.rest.request.LoginWithFacebookRequest;
import kmitl.ce.smart_music_player.models.rest.response.SearchResponse;
import kmitl.ce.smart_music_player.models.rest.response.UserResponse;
import kmitl.ce.smart_music_player.models.rest.response.base.BaseResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Jo on 4/4/2017.
 */

public interface ApiInterface {

    @POST("api/v1/smp/login/facebook")
    Call<BaseResponse<UserResponse>> loginWithFacebook(@Body LoginWithFacebookRequest request);

    @GET("api/v1/smp/login/new")
    Call<BaseResponse<UserResponse>> localLogin();

    @GET("api/v1/smp/songs/random/{qty}")
    Call<BaseResponse<List<Music>>> getRandomMusic(@Path("qty") int qty);

    @GET("api/v1/smp/songs/suggested/{userId}/{qty}")
    Call<BaseResponse<List<Music>>> getSuggestedMusics(@Path("userId") String userId, @Path("qty") int qty);

    @GET("api/v1/smp/songs/search/{keyword}")
    Call<BaseResponse<SearchResponse>> searchSongs(@Path("keyword") String keyword);

    @GET("api/v1/smp/songs/{musicIds}")
    Call<BaseResponse<List<Music>>> getMusicLists(@Path("musicIds")String musicIds);
}
