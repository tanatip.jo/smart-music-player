package kmitl.ce.smart_music_player.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.ui.adapters.SuggesionAdapter;

/**
 * Created by Dell on 23/3/2560.
 */

public class SuggestedMusicsFragment extends Fragment {
    private List<Music> suggestedMusics;


    public static SuggestedMusicsFragment newInstance(List<Music> suggestedMusics) {
        SuggestedMusicsFragment fragment = new SuggestedMusicsFragment();
        fragment.setSuggestedMusics(suggestedMusics);
        return fragment;
    }

    public SuggestedMusicsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_suggesion_music, container, false);
        
        RecyclerView mRecyclerView;
        RecyclerView.Adapter mAdapter;

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(100);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new SuggesionAdapter(getActivity() , suggestedMusics);

        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

    public void setSuggestedMusics(List<Music> suggestedMusics) {
        this.suggestedMusics = suggestedMusics;
    }

}
