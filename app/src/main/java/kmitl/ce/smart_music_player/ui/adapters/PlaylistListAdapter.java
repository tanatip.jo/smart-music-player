package kmitl.ce.smart_music_player.ui.adapters;

/**
 * Created by Dell on 23/3/2560.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.Realm;
import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.realm.RealmPlaylists;
import kmitl.ce.smart_music_player.ui.activities.MainActivity;
import kmitl.ce.smart_music_player.ui.fragments.PlaylistDetailFragment;
import kmitl.ce.smart_music_player.utils.StringEditorUtil;


public class PlaylistListAdapter extends RecyclerView.Adapter<PlaylistListAdapter.PlaylistViewHolder> {
    private Context mContext;
    private Realm realm;

    public PlaylistListAdapter(Context context, Realm realm) {
        this.mContext = context;
        this.realm = realm;
    }


    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_playlist, null);
        PlaylistViewHolder vh = new PlaylistViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final PlaylistViewHolder holder, int position) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        holder.textView.setWidth((screenWidth) * 80 / 100);
        holder.imageView.setMaxWidth((screenWidth) * 20 / 100);

        RealmPlaylists realmPlaylist = realm.where(RealmPlaylists.class).findAll().get(position);

        holder.textView.setText(StringEditorUtil.subStringMusicTitle(realmPlaylist.getPlaylistName(), 2));

//        byte[] thumbnail = musicInformationList.get(position).getThumbnail();
//        byte[] thumbnail = realmPlaylist.getThumnail();
//        if (thumbnail != null) {
//            Bitmap bitmap = BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
////            holder.imageView.setImageBitmap(bitmap);
//            int sizePx = convertDpToPixel(50);
//            holder.imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, sizePx, sizePx, false));
//        } else {
            Picasso.with(mContext).load(R.drawable.musical_note).into(holder.imageView);
//        }

        holder.textView.setTag(holder);
        holder.imageView.setTag(holder);
        holder.all.setTag(holder);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistViewHolder musicViewHolder = (PlaylistViewHolder) v.getTag();

                int position = musicViewHolder.getAdapterPosition();

                PlaylistDetailFragment fragment = PlaylistDetailFragment.newInstance(position);

                FragmentTransaction transaction = ((MainActivity)mContext).getSupportFragmentManager().beginTransaction();
                Fragment prevFragment = ((MainActivity)mContext).getSupportFragmentManager().findFragmentByTag("playlistDetailFragment");
                if (prevFragment != null) {
                    transaction.remove(prevFragment);
                }
                transaction.addToBackStack(null);
                fragment.show(transaction, "playlistDetailFragment");
//                transaction.replace(R.id.contentFragment, fragment);
//                transaction.commit();
//
////                Toast.makeText(mContext,musicInformationList.get(position).getTitle(), Toast.LENGTH_SHORT).show();
//                ((MainActivity) mContext).playMusic(position);


//                PlaylistDetailAdapter.MusicViewHolder musicViewHolder = (PlaylistDetailAdapter.MusicViewHolder) v.getTag();
//
//                int position = musicViewHolder.getAdapterPosition();
//
//                RealmPlaylists obj= realm.where(RealmPlaylists.class)
//                        .findAll()
//                        .get(position);

//                PlaylistDetailFragment df= new PlaylistDetailFragment().newInstance(obj);
//                df.show(getFragment(), "musicPlayingDialog");
            }
        };
        holder.textView.setOnClickListener(onClickListener);
        holder.imageView.setOnClickListener(onClickListener);
        holder.all.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return realm.where(RealmPlaylists.class).findAll().size();
    }

    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout all;

        public PlaylistViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.music_image);
            this.textView = (TextView) view.findViewById(R.id.music_title);
            all = (RelativeLayout) view.findViewById(R.id.all);
        }
    }

//    private int convertDpToPixel(int dp) {
//        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
//        int px = (int) (dp * (metrics.densityDpi / 160f));
//        return px;
//    }
}

