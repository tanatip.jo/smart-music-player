package kmitl.ce.smart_music_player.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import kmitl.ce.smart_music_player.ui.activities.MainActivity;

/**
 * Created by Jo on 4/7/2017.
 */

public class ErrorDialog {
    private static AlertDialog alertDialog;


    private static AlertDialog createAlertDialog(Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Error");
        builder.setMessage(msg);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog = null;
            }
        });
        return builder.create();
    }

    public static void showErrorDialog(Context context,String msg) {
        if (alertDialog == null || !alertDialog.isShowing()) {
            alertDialog = createAlertDialog(context,msg);
            alertDialog.show();
        }
    }
}
