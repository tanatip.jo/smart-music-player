package kmitl.ce.smart_music_player.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.ui.adapters.MusicsAdapter;

/**
 * Created by Dell on 22/3/2560.
 */

public class MusicsFragment extends Fragment {

    private List<Music> musics;

    public static MusicsFragment newInstance(List<Music> musicList) {
        MusicsFragment fragment = new MusicsFragment();
        fragment.setMusicList(musicList);
        return fragment;
    }

    public MusicsFragment(){
    }

    public void setMusicList(List<Music> Musics) {
        musics = Musics;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_musics, container, false);

        RecyclerView mRecyclerView;
        RecyclerView.Adapter mAdapter;

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(100);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mAdapter = new MusicsAdapter(getActivity(), musics);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;

    }

}
