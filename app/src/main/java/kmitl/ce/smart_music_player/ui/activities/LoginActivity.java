package kmitl.ce.smart_music_player.ui.activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.rest.request.LoginWithFacebookRequest;
import kmitl.ce.smart_music_player.models.rest.response.UserResponse;
import kmitl.ce.smart_music_player.models.rest.response.base.BaseResponse;
import kmitl.ce.smart_music_player.network.ApiClient;
import kmitl.ce.smart_music_player.network.ApiInterface;
import kmitl.ce.smart_music_player.ui.dialog.ErrorDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginActivity extends AppCompatActivity {


    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private Button continueLogin;
    public static String LOGIN_USER_ID_PREFERENCE = "LOGIN_USER_ID_PREFERENCE";
    private AccessToken accessToken;
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefsEditor = appSharedPrefs.edit();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();

        if (isLoggedIn()) {
            onServerLoginWithFacebook();
        }

        // Callback registration -- First activity_login with facebook
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                onServerLoginWithFacebook();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "LoginActivity cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                String title = getString(R.string.error);
                String alertMessage = exception.getMessage();
                showResult(title, alertMessage);
                Toast.makeText(getApplicationContext(), "LoginActivity Error", Toast.LENGTH_SHORT).show();
            }
        });


        this.continueLogin = (Button) findViewById(R.id.local_login);
        this.continueLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onServerLocalLogin();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void onServerLoginWithFacebook() {
        LoginWithFacebookRequest request = new LoginWithFacebookRequest();
        request.setFacebookId(accessToken.getUserId());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseResponse<UserResponse>> call = apiService.loginWithFacebook(request);
        call.enqueue(new Callback<BaseResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserResponse>> call, Response<BaseResponse<UserResponse>> response) {
                if (response.isSuccessful()) {
                    loginSuccess(response.body().getData().getUserId());
                } else {
                    // throw new Exception
                    ErrorDialog.showErrorDialog(LoginActivity.this,"Server response failed");
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserResponse>> call, Throwable t) {
                // ! do nothing
                // ! implement soon!!!
//                    throw new NetworkErrorException("Cannot connect to Server");
                ErrorDialog.showErrorDialog(LoginActivity.this,"Cannot connect to Server");
            }
        });
    }

    private void onServerLocalLogin() {
        String userId = PreferenceManager.getDefaultSharedPreferences(this).getString(LoginActivity.LOGIN_USER_ID_PREFERENCE, "");
        if(userId.equals("")) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<BaseResponse<UserResponse>> call = apiService.localLogin();
            call.enqueue(new Callback<BaseResponse<UserResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<UserResponse>> call, Response<BaseResponse<UserResponse>> response) {
                    if (response.isSuccessful()) {
                        loginSuccess(response.body().getData().getUserId());
                    } else {
                        ErrorDialog.showErrorDialog(LoginActivity.this, "Server response failed");
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<UserResponse>> call, Throwable t) {
                    // do nothing now
                    // implement soon
                    ErrorDialog.showErrorDialog(LoginActivity.this, "Cannot connect to Server");
                }
            });
        } else {
            loginSuccess(userId);
        }
    }

    private void loginSuccess(String pref) {
        prefsEditor.putString(LOGIN_USER_ID_PREFERENCE, pref);
        prefsEditor.apply();

        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        finish();
        startActivity(i);
    }

    private void showResult(String title, String alertMessage) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(alertMessage)
                .setPositiveButton("OK", null)
                .show();
    }

    public boolean isLoggedIn() {
        this.accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
