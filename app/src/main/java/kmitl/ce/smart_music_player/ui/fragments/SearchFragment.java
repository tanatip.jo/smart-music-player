package kmitl.ce.smart_music_player.ui.fragments;

/**
 * Created by Dell on 25/3/2560.
 */

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.models.rest.response.SearchResponse;
import kmitl.ce.smart_music_player.models.rest.response.base.BaseResponse;
import kmitl.ce.smart_music_player.network.ApiClient;
import kmitl.ce.smart_music_player.network.ApiInterface;
import kmitl.ce.smart_music_player.ui.activities.MainActivity;
import kmitl.ce.smart_music_player.ui.adapters.SearchResultsAdapter;
import kmitl.ce.smart_music_player.ui.dialog.ErrorDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 23/3/2560.
 */

public class SearchFragment extends DialogFragment {

    private List<Music> searchMusicResults;
    private String keyword;


    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        ImageView searchBtn = (ImageView) rootView.findViewById(R.id.search_btn);
        ImageView backBtn = (ImageView) rootView.findViewById(R.id.back);
        final EditText searchBar = (EditText) rootView.findViewById(R.id.search_bar);

//        searchBar.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.onTouchEvent(event);
//                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                if (imm != null) {
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//                return true;
//            }
//        });

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(100);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return false;
            }
        };

        rootView.setOnTouchListener(touchListener);
        recyclerView.setOnTouchListener(touchListener);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                onDestroy();
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyword = searchBar.getText().toString();
                searchMusic(keyword);
            }
        });


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void searchMusic(String keyword) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseResponse<SearchResponse>> call = apiService.searchSongs(keyword);
        call.enqueue(new Callback<BaseResponse<SearchResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<SearchResponse>> call, Response<BaseResponse<SearchResponse>> response) {
                if (response.isSuccessful()) {
                    List<Music> byName = response.body().getData().getSearchByName();
                    byName = byName.subList(0, Math.min(byName.size(), 6));

                    List<Music> byArtists = response.body().getData().getSearchByArtists();
                    byArtists = byArtists.subList(0, Math.min(byArtists.size(), 6));

                    byName.addAll(byArtists);
                    searchMusicResults = byName;
                    ((MainActivity) getActivity()).setSearchMusicList(searchMusicResults);

                    adapter = new SearchResultsAdapter(getActivity(), searchMusicResults);
                    recyclerView.setAdapter(adapter);

                } else {
                    ErrorDialog.showErrorDialog( ((MainActivity)getActivity()),"Server response failed");
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<SearchResponse>> call, Throwable t) {
                ErrorDialog.showErrorDialog( ((MainActivity)getActivity()),"Cannot connect to Server");
            }
        });
    }

}
