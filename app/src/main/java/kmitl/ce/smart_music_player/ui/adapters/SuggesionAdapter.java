package kmitl.ce.smart_music_player.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.ui.activities.MainActivity;
import kmitl.ce.smart_music_player.utils.StringEditorUtil;

/**
 * Created by Dell on 23/3/2560.
 */

public class SuggesionAdapter extends RecyclerView.Adapter<SuggesionAdapter.PlaylistViewHolder> {
    private Context mContext;
    private List<Music> suggestedMusics;

    public SuggesionAdapter(Context context, List<Music> suggestedMusics) {
        this.mContext = context;
        this.suggestedMusics = suggestedMusics;
    }

    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_suggesion_music, null);

        SuggesionAdapter.PlaylistViewHolder vh = new SuggesionAdapter.PlaylistViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SuggesionAdapter.PlaylistViewHolder holder, int position) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        holder.textView.setWidth((screenWidth) * 80 / 100);
        holder.imageView.setMaxWidth((screenWidth) * 20 / 100);

        Music suggestedMusic = suggestedMusics.get(position);

        holder.textView.setText(StringEditorUtil.subStringMusicTitle(setTextModified(suggestedMusic.getName()), 2));

        Picasso.with(mContext).load(R.drawable.musical_note).into(holder.imageView);

        holder.textView.setTag(holder);
        holder.imageView.setTag(holder);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SuggesionAdapter.PlaylistViewHolder PlaylistViewHolder = (SuggesionAdapter.PlaylistViewHolder) v.getTag();
                int position = PlaylistViewHolder.getAdapterPosition();
                ((MainActivity) mContext).setSuggested();
                ((MainActivity) mContext).playMusic(position);
            }
        };

        holder.textView.setOnClickListener(onClickListener);
        holder.imageView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return suggestedMusics.size();
    }

    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public PlaylistViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.music_image);
            this.textView = (TextView) view.findViewById(R.id.music_title);
        }
    }

    private String setTextModified(String str) {
        return str.length() > 20 ? str.substring(0, 20) + "..." : str;
    }
}
