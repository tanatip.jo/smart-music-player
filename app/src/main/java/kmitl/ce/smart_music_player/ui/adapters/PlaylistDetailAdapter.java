package kmitl.ce.smart_music_player.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.realm.Realm;
import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.ui.activities.MainActivity;
import kmitl.ce.smart_music_player.utils.StringEditorUtil;

/**
 * Created by Jo on 8/16/2016.
 */
public class PlaylistDetailAdapter extends RecyclerView.Adapter<PlaylistDetailAdapter.MusicViewHolder> {
    private Context mContext;
    private Realm realm;
    private List<Music> musics;

    public PlaylistDetailAdapter(Context context, Realm realm, List<Music> musics) {
        this.mContext = context;
        this.realm = realm;
        this.musics = musics;
    }

    @Override
    public MusicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_musics, null);

        MusicViewHolder vh = new MusicViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MusicViewHolder holder, int position) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        holder.textView.setWidth((screenWidth) * 80 / 100);
        holder.imageView.setMaxWidth((screenWidth) * 20 / 100);

        Music music = musics.get(position);

        holder.textView.setText(StringEditorUtil.subStringMusicTitle(music.getName(), 2));
        Picasso.with(mContext).load(R.drawable.musical_note).into(holder.imageView);

        holder.textView.setTag(holder);
        holder.imageView.setTag(holder);
        holder.artistName.setTag(holder);
        holder.all.setTag(holder);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistDetailAdapter.MusicViewHolder musicViewHolder = (PlaylistDetailAdapter.MusicViewHolder) v.getTag();
                int position = musicViewHolder.getAdapterPosition();
                ((MainActivity) mContext).setOnPlaylist();
                ((MainActivity) mContext).playMusic(position);
            }
        };

        holder.textView.setOnClickListener(onClickListener);
        holder.imageView.setOnClickListener(onClickListener);
        holder.artistName.setOnClickListener(onClickListener);
        holder.all.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return musics.size();
    }

    public static class MusicViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public TextView artistName;
        public RelativeLayout all;

        public MusicViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.music_image);
            this.textView = (TextView) view.findViewById(R.id.music_title);
            artistName = (TextView ) view.findViewById(R.id.music_artist);
            all = (RelativeLayout) view.findViewById(R.id.all);
        }
    }
}
