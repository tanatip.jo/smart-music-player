package kmitl.ce.smart_music_player.ui.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.models.realm.RealmPlaylistDetail;
import kmitl.ce.smart_music_player.models.realm.RealmPlaylists;
import kmitl.ce.smart_music_player.models.rest.response.SearchResponse;
import kmitl.ce.smart_music_player.models.rest.response.base.BaseResponse;
import kmitl.ce.smart_music_player.network.ApiClient;
import kmitl.ce.smart_music_player.network.ApiInterface;
import kmitl.ce.smart_music_player.ui.activities.MainActivity;
import kmitl.ce.smart_music_player.ui.adapters.PlaylistDetailAdapter;
import kmitl.ce.smart_music_player.ui.adapters.SearchResultsAdapter;
import kmitl.ce.smart_music_player.ui.dialog.ErrorDialog;
import kmitl.ce.smart_music_player.utils.StringEditorUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 23/3/2560.
 */

public class PlaylistDetailFragment extends DialogFragment {

    private ImageView imageView;
    private TextView playlistName;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private Realm realm;
    private int playlistId;

    public static PlaylistDetailFragment newInstance(int playlistId) {
        PlaylistDetailFragment fragment = new PlaylistDetailFragment();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme);
        fragment.setPlaylistId(playlistId);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.realm = Realm.getDefaultInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frament_playlist_detail, container, false);

        ImageButton backBtn = (ImageButton) rootView.findViewById(R.id.back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                onDestroy();
            }
        });

        ImageButton shuffleBtn = (ImageButton) rootView.findViewById(R.id.shuffle);
        shuffleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setOnPlaylist();
                ((MainActivity) getActivity()).setShuffle(true);
                ((MainActivity) getActivity()).setOnPlaylist();
                ((MainActivity) getActivity()).playMusic(0);
            }
        });

        imageView = (ImageView) rootView.findViewById(R.id.imageView);
        playlistName = (TextView) rootView.findViewById(R.id.playlist_name);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(100);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        String playlistNameString = realm.where(RealmPlaylists.class).findAll().get(playlistId).getPlaylistName();
        playlistName.setText(playlistNameString);
        setImageView();

        List<RealmPlaylistDetail> realmPlaylistDetails = realm.where(RealmPlaylistDetail.class).equalTo("playlistId", playlistId).findAll();

        List<String> musicIds = new ArrayList<>();
        for (RealmPlaylistDetail realmPlaylistDetail : realmPlaylistDetails) {
            musicIds.add(realmPlaylistDetail.getMusicId());
        }

        getPlaylistMusics(musicIds);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    private void setImageView() {
        //ImageView
//        byte[] thumbnail = ((MainActivity) getActivity()).getRealmMusicInformation().getThumnail();
//        byte[] thumbnail = playlistInformation.getThumnail();
//        if (thumbnail != null) {
//            Bitmap bitmap = BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
//            imageView.setImageBitmap(bitmap);
//        } else {
        Picasso.with(getActivity()).load(R.drawable.view_image).into(imageView);
//        }
    }

    private void getPlaylistMusics(List<String> musicIds) {
        if (musicIds.size() > 0) {
            String musicIdsString = StringEditorUtil.joinStringList(musicIds, ",");
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<BaseResponse<List<Music>>> call = apiService.getMusicLists(musicIdsString);
            call.enqueue(new Callback<BaseResponse<List<Music>>>() {
                @Override
                public void onResponse(Call<BaseResponse<List<Music>>> call, Response<BaseResponse<List<Music>>> response) {
                    if (response.isSuccessful()) {
                        List<Music> musics = response.body().getData();
                        ((MainActivity) getActivity()).setPlaylistMusics(musics);
                        mAdapter = new PlaylistDetailAdapter(getActivity(), realm, musics);
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        ErrorDialog.showErrorDialog( ((MainActivity)getActivity()),"Server response failed");
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<List<Music>>> call, Throwable t) {
                    ErrorDialog.showErrorDialog( ((MainActivity)getActivity()),"Cannot connect to Server");
                }
            });
        } else {
            List<Music> musics = new ArrayList<>();
            ((MainActivity) getActivity()).setPlaylistMusics(musics);
            mAdapter = new PlaylistDetailAdapter(getActivity(), realm, musics);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
