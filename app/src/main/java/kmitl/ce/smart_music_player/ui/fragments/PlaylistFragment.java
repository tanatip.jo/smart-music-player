package kmitl.ce.smart_music_player.ui.fragments;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import io.realm.Realm;
import io.realm.RealmQuery;
import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.realm.RealmPlaylists;
import kmitl.ce.smart_music_player.ui.adapters.PlaylistListAdapter;

/**
 * Created by Dell on 22/3/2560.
 */

public class PlaylistFragment extends Fragment {

    private Realm mRealm;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

//    @Override
//    public void onItemClicked(View v) {
//
//        PlaylistListAdapter.PlaylistViewHolder playlistViewHolder = (PlaylistListAdapter.PlaylistViewHolder) v.getTag();
//
//        int position = playlistViewHolder.getAdapterPosition();
//
//        RealmPlaylists realmPlaylistInformation= realm.where(RealmPlaylists.class)
//                .findAll()
//                .get(position);
//
//        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
//        Gson gson = new Gson();
//        String json = appSharedPrefs.getString(realmPlaylistInformation.getPlaylistName(), "");
//        PlaylistInformation obj = gson.fromJson(json, PlaylistInformation.class);
//        RealmList<RealmMusicInformation> listMusicInformation = new RealmList<RealmMusicInformation>();
//
//        if(obj!=null & obj.getSongs()!=null){
//            for (int i=0; i<obj.getSongs().length;i++){
//                listMusicInformation.add(i,realm.where(RealmMusicInformation.class)
//                        .equalTo("id",Integer.parseInt(obj.getSongs()[i])).findFirst());
//            }
//        }else{
//
//        }
//
//        PlaylistDetailFragment df= new PlaylistDetailFragment()
//                .newInstance(realmPlaylistInformation,listMusicInformation);
//        df.show(getFragmentManager(), "musicPlayingDialog");
//    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_playlist, container, false);

        mRealm = Realm.getDefaultInstance();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(100);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        final Dialog dialog = createPlaylistsDialog();

        TextView addNewPlaylist = (TextView) rootView.findViewById(R.id.addNewPlaylist);
        addNewPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        mAdapter = new PlaylistListAdapter(getActivity(), mRealm);
        mRecyclerView.setAdapter(this.mAdapter);

        return rootView;
    }

    private void savePlaylist(final String name) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm r) {
                RealmQuery<RealmPlaylists> query = mRealm.where(RealmPlaylists.class);
                RealmPlaylists rmif = r.createObject(RealmPlaylists.class, query.findAll().size());
                rmif.setPlaylistName(name);
            }
        });
    }

    private Dialog createPlaylistsDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Add New Playlist");
        dialog.setContentView(R.layout.dialog_create_playlist);

        Button buttonCancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button buttonOK = (Button) dialog.findViewById(R.id.button_ok);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText newPlaylist = (EditText) dialog.findViewById(R.id.playlist_name);
                savePlaylist(newPlaylist.getText().toString());
                dialog.dismiss();
            }
        });
        return dialog;
    }
}