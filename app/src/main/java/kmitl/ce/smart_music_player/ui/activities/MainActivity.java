package kmitl.ce.smart_music_player.ui.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import io.realm.Realm;
import kmitl.ce.smart_music_player.R;
import kmitl.ce.smart_music_player.models.Music;
import kmitl.ce.smart_music_player.models.rest.response.base.BaseResponse;
import kmitl.ce.smart_music_player.network.ApiClient;
import kmitl.ce.smart_music_player.network.ApiInterface;
import kmitl.ce.smart_music_player.ui.dialog.ErrorDialog;
import kmitl.ce.smart_music_player.ui.fragments.MusicsFragment;
import kmitl.ce.smart_music_player.ui.fragments.MusicPlayerFragment;
import kmitl.ce.smart_music_player.ui.fragments.PlaylistFragment;
import kmitl.ce.smart_music_player.ui.fragments.SearchFragment;
import kmitl.ce.smart_music_player.ui.fragments.SuggestedMusicsFragment;
import kmitl.ce.smart_music_player.utils.StringEditorUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    // model variable
//    private Realm realm;
    private MediaPlayer mediaPlayer;
    private Integer currentPosition;
    private Integer currentMusicPlaying;
    private Integer currentMusicIndex;
    private boolean isRepeat = false;
    private boolean isShuffle = false;
    private boolean isSuggested = false;
    private boolean isSearched = false;
    private boolean isOnPlaylist = false;
    private List<Integer> normalIndexList;
    private List<Integer> activeIndexList;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private List<Music> musicList;
    private List<Music> suggestedMusicList;
    private List<Music> searchMusicList;
    private List<Music> playlistMusics;
    private int musicSize;

    // ui variable
    private ImageButton musicPlayingButton;
    private MusicsFragment musicsFragment;
    private Integer playStateImage = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        realm = Realm.getDefaultInstance();
        initialActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mediaPlayer.stop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    setUpActivity();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Read external Storage Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void playMusic(int position) {
        currentMusicPlaying = position;
        currentMusicIndex = (isShuffle) ? activeIndexList.indexOf(position) : position;
        play();
        setPlayToolBar();
    }

    public void initialActivity() {
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissionGetAllMusics();
        } else {
            setUpActivity();
        }
    }

    public void play() {
        Music music = getMusic();
        mediaPlayer.reset();
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mp.reset();
                return false;
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(music.getStreamingUrl());
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playStateClick(ImageButton imageButton) {
        try {
            if (mediaPlayer.isPlaying()) {
                playStateImage = R.drawable.play_button;
                mediaPlayer.pause();
                currentPosition = mediaPlayer.getCurrentPosition();
            } else {
                playStateImage = R.drawable.pause_button;
                mediaPlayer.seekTo(currentPosition);
                mediaPlayer.start();
            }
            updatePlayButton(imageButton);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePlayButton(ImageButton imageButton) {
        Picasso.with(MainActivity.this).load(playStateImage).into(imageButton);
    }

    public void nextSong() {
        currentMusicIndex = (currentMusicIndex + 1 >= musicSize) ?
                0 : currentMusicIndex + 1;
        currentMusicPlaying = activeIndexList.get(currentMusicIndex);
        play();
    }

    public void previousSong() {
        currentMusicIndex = (currentMusicIndex - 1 < 0) ?
                activeIndexList.size() : currentMusicIndex - 1;
        currentMusicPlaying = activeIndexList.get(currentMusicIndex);
        play();
    }

    public void setOnPlaylist() {
        resetActiveList();
        isOnPlaylist = true;
        musicSize = playlistMusics.size();
    }

    public void setSuggested() {
        resetActiveList();
        isSuggested = true;
        musicSize = suggestedMusicList.size();
    }

    public void setSearched() {
        resetActiveList();
        isSearched = true;
        musicSize = searchMusicList.size();
    }

    public void setRandom() {
        resetActiveList();
        musicSize = musicList.size();
    }

    private void resetActiveList() {
        isSearched = false;
        isSuggested = false;
        isOnPlaylist = false;
    }

    public void setRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }

    public void setShuffle(boolean isShuffle) {
        this.isShuffle = isShuffle;
        if (isShuffle) {
            List<Integer> shuffleIndexList = new ArrayList<>();
            int count = musicSize;
            for (int i = 0; i < count; i++) {
                shuffleIndexList.add(i);
            }
            Collections.shuffle(shuffleIndexList);
            activeIndexList = shuffleIndexList;
        } else {
            currentMusicIndex = currentMusicPlaying;
            activeIndexList = normalIndexList;
        }
    }

    public void setSearchMusicList(List<Music> searchMusicList) {
        this.searchMusicList = searchMusicList;
    }

    public void setPlaylistMusics(List<Music> playlistMusics) {
        this.playlistMusics = playlistMusics;
    }

    public int getPlayStateImage() {
        return playStateImage;
    }

    public ImageButton getMusicPlayingButton() {
        return musicPlayingButton;
    }

    public Music getMusic() {
        return (currentMusicPlaying != null) ?
                (isSuggested) ?
                        suggestedMusicList.get(currentMusicPlaying) :
                        (isSearched) ?
                                searchMusicList.get(currentMusicPlaying) :
                                (isOnPlaylist) ?
                                        playlistMusics.get(currentMusicPlaying) :
                                        musicList.get(currentMusicPlaying) :
                null;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public void updateMusicName() {
        TextView musicPlayingTitle = (TextView) findViewById(R.id.music_playing_title);
        musicPlayingTitle.setText(StringEditorUtil.subStringMusicTitle(getMusic().getName(), 0));
    }

    private void checkPermissionGetAllMusics() {
        int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        } else {
            setUpActivity();
        }
    }

    private void getRandomMusics() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseResponse<List<Music>>> call = apiService.getRandomMusic(20);
        call.enqueue(new Callback<BaseResponse<List<Music>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Music>>> call, Response<BaseResponse<List<Music>>> response) {
                if (response.isSuccessful()) {
                    musicList = response.body().getData();
                    refreshMusics();
                } else {
                    ErrorDialog.showErrorDialog(MainActivity.this,"Server response failed");
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Music>>> call, Throwable t) {
                ErrorDialog.showErrorDialog(MainActivity.this,"Cannot connect to Server");
            }
        });
    }

    private void getSuggestedMusic() {
        String userId = PreferenceManager.getDefaultSharedPreferences(this).getString(LoginActivity.LOGIN_USER_ID_PREFERENCE, "");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseResponse<List<Music>>> call = apiService.getSuggestedMusics(userId, 8);
        call.enqueue(new Callback<BaseResponse<List<Music>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Music>>> call, Response<BaseResponse<List<Music>>> response) {
                if (response.isSuccessful()) {
                    suggestedMusicList = response.body().getData();
                    refreshSuggestedMusics();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Music>>> call, Throwable t) {
                ErrorDialog.showErrorDialog(MainActivity.this,"Cannot connect to Server");
//                Toast.makeText(getApplicationContext(), "api call fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setPlayToolBar() {
        playStateImage = R.drawable.pause_button;
        updatePlayButton(musicPlayingButton);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        int screenWidth = displaymetrics.widthPixels;

        TextView musicPlayingTitle = (TextView) findViewById(R.id.music_playing_title);
        musicPlayingTitle.setWidth((screenWidth) * 70 / 100);

        musicPlayingTitle.setText(StringEditorUtil.subStringMusicTitle(getMusic().getName(), 0));

        Toolbar musicPlayingBar = (Toolbar) findViewById(R.id.music_list_playing);
        setSupportActionBar(musicPlayingBar);

//        holder.textView.setWidth((screenWidth) * 80 / 100);
//        holder.imageView.setMaxWidth((screenWidth) * 20 / 100);
        musicPlayingBar.setVisibility(View.VISIBLE);

        musicPlayingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prevFragment = getSupportFragmentManager().findFragmentByTag("musicPlayingDialog");
                if (prevFragment != null) {
                    ft.remove(prevFragment);
                }

                ft.addToBackStack(null);
                MusicPlayerFragment musicPlayerFragment = MusicPlayerFragment.newInstance(getMusic());
                musicPlayerFragment.show(ft, "musicPlayingDialog");
            }
        });

        musicPlayingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playStateClick(musicPlayingButton);
            }
        });
    }

    private void refreshMusics() {
        normalIndexList = new ArrayList<>();
        int count = musicList.size();
        for (int i = 0; i < count; i++) {
            normalIndexList.add(i);
        }
        activeIndexList = normalIndexList;
        musicSize = activeIndexList.size();

        musicsFragment = MusicsFragment.newInstance(musicList);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentFragment, musicsFragment);
        transaction.commit();
    }

    private void refreshSuggestedMusics() {
        int suggestionId = R.id.contentFragmentSuggesion;
        if (suggestedMusicList != null && suggestedMusicList.size() > 0) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            SuggestedMusicsFragment suggestedMusicsFragment = SuggestedMusicsFragment.newInstance(suggestedMusicList);
            transaction.replace(suggestionId, suggestedMusicsFragment);
            transaction.commit();
        } else {
            FrameLayout suggestionArea = (FrameLayout) findViewById(suggestionId);
            suggestionArea.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpActivity() {
        musicPlayingButton = (ImageButton) findViewById(R.id.music_playing_button);
        musicPlayingButton.setMaxWidth((new DisplayMetrics().widthPixels) * 30 / 100);

        initTabs();
        initAleartDialog();
        initMediaPlayer();

        ImageView refreshButton = (ImageView) findViewById(R.id.refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSuggestedMusic();
                getRandomMusics();
            }
        });

        //set visible suggestion fragment
        getSuggestedMusic();

        // call refreshMusics
        getRandomMusics();
    }

    private void initTabs() {
        SegmentedGroup segment = (SegmentedGroup) findViewById(R.id.segmented_control);
        segment.setTintColor(Color.parseColor("#a39b9b"), Color.parseColor("#ffffff"));
        RadioButton musicBtn = (RadioButton) findViewById(R.id.songs);
        RadioButton playlistBtn = (RadioButton) findViewById(R.id.playlsits);

        //Set listener for button
        musicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicsFragment = MusicsFragment.newInstance(musicList);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentFragment, musicsFragment);
                transaction.commit();
            }
        });

        playlistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistFragment playlistFragment = new PlaylistFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentFragment, playlistFragment);
                transaction.commit();
            }
        });

        segment.check(R.id.songs);
    }

    private void initMediaPlayer() {
        mediaPlayer = new MediaPlayer();

        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return true;
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                Integer toIndex = null;
                if (currentMusicIndex + 1 >= activeIndexList.size()) {
                    if (isRepeat()) {
                        toIndex = 0;
                    }
                } else {
                    toIndex = currentMusicIndex + 1;
                }
                if (toIndex != null) {
                    playMusic(toIndex);
                }
            }
        });
    }

    private void initAleartDialog() {
        final AlertDialog.Builder builder = createDialogBuilder();
        ImageView etc = (ImageView) findViewById(R.id.etc);
        etc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.show();
            }
        });
    }

    private AlertDialog.Builder createDialogBuilder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] list = new String[]{"Search", "Log out"};
        final int[] mSelected = {0};

        builder.setTitle("Select");
        builder.setSingleChoiceItems(list, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSelected[0] = which;
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // ส่วนนี้สำหรับเซฟค่าลง database หรือ SharedPreferences.
                dialog.dismiss();
                if (mSelected[0] == 0) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    Fragment prevFragment = getSupportFragmentManager().findFragmentByTag("searchFragment");
                    if (prevFragment != null) {
                        ft.remove(prevFragment);
                    }
                    ft.addToBackStack(null);
                    SearchFragment searchFragment = SearchFragment.newInstance();
                    searchFragment.show(ft, "searchFragment");
                } else {
                    LoginManager.getInstance().logOut();
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    finish();
                    startActivity(i);
                }
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.create();

        return builder;
    }
}