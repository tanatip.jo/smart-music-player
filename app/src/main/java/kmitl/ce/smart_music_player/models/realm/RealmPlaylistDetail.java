package kmitl.ce.smart_music_player.models.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Data;

/**
 * Created by Jo on 4/6/2017.
 */
@Data
public class RealmPlaylistDetail extends RealmObject{
    @PrimaryKey
    private int id;

    private int playlistId;

    private String musicId;
}
