package kmitl.ce.smart_music_player.models.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Data;

/**
 * Created by Dell on 25/3/2560.
 */
@Data
public class RealmPlaylists extends RealmObject {
    @PrimaryKey
    private int id;
    private String playlistName;
}
