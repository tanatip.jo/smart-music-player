package kmitl.ce.smart_music_player.models.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.List;

import kmitl.ce.smart_music_player.models.Music;
import lombok.Data;

/**
 * Created by Jo on 4/6/2017.
 */
@Data
public class SearchResponse {
    @JsonProperty("by_name")
    private List<Music> searchByName;

    @JsonProperty("by_artists")
    private List<Music> searchByArtists;
}
